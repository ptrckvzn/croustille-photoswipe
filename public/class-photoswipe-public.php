<?php

namespace Croustille\PhotoSwipe;

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Plugin
 * @subpackage Plugin/public
 * @author     Your Name <email@example.com>
 */
class Frontend {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

        $load_styles = apply_filters('croustille/photoswipe_styles', true);

        if ($load_styles) { // load les styles en dev (inline avec dans public.js)
            wp_enqueue_style($this->plugin_name, asset_path('styles/public.css'), array(), $this->version, 'all');
        }

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		wp_enqueue_script($this->plugin_name, asset_path('scripts/public.js'), [], null, true);

        wp_localize_script($this->plugin_name, 'Croustille_PhotoSwipe', [
            'containerClass' => apply_filters('croustile/photoswipe_container_class', 'entry-content'),
        ]);

    }

    public function dom_element () {
        if (Croustille_PhotoSwipe()->use_photoswipe()) {
            ob_start();

            require plugin_dir_path( dirname( __FILE__ ) ) . '/public/partials/photoswipe-html-element.php';
            $template = ob_get_contents();
            $content = $template;

            ob_end_clean();
            echo $content;
        }
    }

}
