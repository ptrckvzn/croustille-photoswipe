/* globals Croustille_PhotoSwipe_Options */

// <div class="my-gallery" itemscope itemtype="http://schema.org/ImageGallery">
//
//   <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
//   <a href="large-image.jpg" itemprop="contentUrl" data-size="600x400">
//   <img src="small-image.jpg" itemprop="thumbnail" alt="Image description" />
//   </a>
//   <figcaption itemprop="caption description">Image caption</figcaption>
// </figure>
//
// <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
//   <a href="large-image.jpg" itemprop="contentUrl" data-size="600x400">
//   <img src="small-image.jpg" itemprop="thumbnail" alt="Image description" />
//   </a>
//   <figcaption itemprop="caption description">Image caption</figcaption>
// </figure>
//
// </div>

// <a href="/app/uploads/2018/01/Obey_-Furlong3_1000.jpg">
//   <img class="lozad wp-image-217" src="/app/uploads/2018/01/Obey_-Furlong3_1000.jpg" width="1000" height="666" data-src="" data-srcset="" data-loaded="true">
//   </a>

// <figure id="attachment_218" style="max-width: 600px" class="wp-caption alignnone">
//  <a class="link-class" href="//localhost:3000/app/uploads/2018/01/Obey_-Furlong4_600.jpg">
//    <img class="image-class wp-image-218 size-full" title="Image Title (no default)" src="//localhost:3000/app/uploads/2018/01/Obey_-Furlong4_600.jpg" alt="This is alt text" width="600" height="441" srcset="/app/uploads/2018/01/Obey_-Furlong4_600.jpg 600w, /app/uploads/2018/01/Obey_-Furlong4_600-75x55.jpg 75w, /app/uploads/2018/01/Obey_-Furlong4_600-400x294.jpg 400w" sizes="(max-width: 600px) 100vw, 600px">
//  </a>
//  <figcaption class="wp-caption-text">
//    This is a caption
//   </figcaption>
// </figure>

import PhotoSwipe from 'photoswipe';
import PhotoSwipeUI_Default from 'photoswipe/dist/photoswipe-ui-default.min';

// export const GALLERY_CLASS = 'article-content';
// export const GALLERY_SELECTOR = `.${GALLERY_CLASS}`;
// export const GALLERY_SELECTOR = `.article-content`;

const findAncestor = (el, cls) => {
  while ((el = el.parentElement) && !el.classList.contains(cls));
  return el;
};

const initPhotoSwipeFromDOM = function (galleryClass) {
  const gallerySelector = '.' + galleryClass;

  // parse slide data (url, title, size ...) from DOM elements
  // (children of gallerySelector)
  const parseThumbnailElements = function (el) {
    const thumbElements = el.getElementsByTagName('figure');
    const numNodes = thumbElements.length;
    const items = [];
    let figureEl;
    let linkEl;
    let size;
    let item;

    // eslint-disable-next-line
    // console.log(thumbElements);

    for (var i = 0; i < numNodes; i++) {

      figureEl = thumbElements[i]; // <figure> element

      // include only element nodes
      if (figureEl.nodeType !== 1) {
        continue;
      }

      linkEl = figureEl.children[0]; // <a> element

      // console.log(figureEl, linkEl.getAttribute('data-size'));
      size = linkEl.getAttribute('data-size').split('x');

      // create slide object
      item = {
        src: linkEl.getAttribute('href'),
        w: parseInt(size[0], 10),
        h: parseInt(size[1], 10),
      };

      if (figureEl.children.length > 1) {
        // <figcaption> content
        item.title = figureEl.children[1].innerHTML;
      }

      if (linkEl.children.length > 0) {
        // <img> thumbnail element, retrieving thumbnail url
        item.msrc = linkEl.children[0].getAttribute('src');
      }

      item.el = figureEl; // save link to element for getThumbBoundsFn
      items.push(item);
    }

    return items;
  };

  // find nearest parent element
  const closest = function closest(el, fn) {
    return el && (fn(el) ? el : closest(el.parentNode, fn));
  };

  // triggers when user clicks on thumbnail
  const onThumbnailsClick = function (e) {
    e = e || window.event;

    // Exit if target is a link and has no data-size attributre
    if (e.target && e.target.tagName && e.target.tagName.toUpperCase() === 'A' && !e.target.hasAttribute('data-size')) {
      return;
    }

    e.preventDefault ? e.preventDefault() : e.returnValue = false;

    const eTarget = e.target || e.srcElement;

    // find root element of slide
    const clickedListItem = closest(eTarget, function (el) {
      return (el.tagName && el.tagName.toUpperCase() === 'FIGURE');
    });

    if (!clickedListItem) {
      return;
    }

    // find index of clicked item by looping through all child nodes
    // alternatively, you may define index via data- attribute
    const clickedGallery = findAncestor(clickedListItem, galleryClass);
    const childNodes = clickedGallery.getElementsByTagName('figure');
    const numChildNodes = childNodes.length;
    let nodeIndex = 0;
    let index;

    // eslint-disable-next-line
    // console.log('clickedGallery', clickedGallery);

    for (var i = 0; i < numChildNodes; i++) {
      if (childNodes[i].nodeType !== 1) {
        continue;
      }

      if (childNodes[i] === clickedListItem) {
        index = nodeIndex;
        break;
      }
      nodeIndex++;
    }



    if (index >= 0) {
      // open PhotoSwipe if valid index found
      openPhotoSwipe(index, clickedGallery);
    }
    return false;
  };

  // parse picture index and gallery index from URL (#&pid=1&gid=2)
  var photoswipeParseHash = function () {
    var hash = window.location.hash.substring(1),
      params = {};

    if (hash.length < 5) {
      return params;
    }

    var vars = hash.split('&');
    for (var i = 0; i < vars.length; i++) {
      if (!vars[i]) {
        continue;
      }
      var pair = vars[i].split('=');
      if (pair.length < 2) {
        continue;
      }
      params[pair[0]] = pair[1];
    }

    if (params.gid) {
      params.gid = parseInt(params.gid, 10);
    }

    return params;
  };

  const openPhotoSwipe = function (index, galleryElement, disableAnimation, fromURL) {
    var pswpElement = document.querySelectorAll('.pswp')[0],
      gallery,
      options,
      items;

    items = parseThumbnailElements(galleryElement);

    // eslint-disable-next-line
    // console.log(items);

    // define options (if needed)
    options = {

      // define gallery index (for URL)
      galleryUID: galleryElement.getAttribute('data-pswp-uid'),

      shareEl: false,

      getThumbBoundsFn: function (index) {
        // See Options -> getThumbBoundsFn section of documentation for more info
        var thumbnail = items[index].el.getElementsByTagName('img')[0], // find thumbnail
          pageYScroll = window.pageYOffset || document.documentElement.scrollTop,
          rect = thumbnail.getBoundingClientRect();

        return { x: rect.left, y: rect.top + pageYScroll, w: rect.width };
      },

    };

    if (Croustille_PhotoSwipe_Options) {
      options = Object.assign(Croustille_PhotoSwipe_Options, options);
    }

    // PhotoSwipe opened from URL
    if (fromURL) {
      if (options.galleryPIDs) {
        // parse real index when custom PIDs are used
        // http://photoswipe.com/documentation/faq.html#custom-pid-in-url
        for (var j = 0; j < items.length; j++) {
          if (items[j].pid == index) {
            options.index = j;
            break;
          }
        }
      } else {
        // in URL indexes start from 1
        options.index = parseInt(index, 10) - 1;
      }
    } else {
      options.index = parseInt(index, 10);
    }

    // exit if index not found
    if (isNaN(options.index)) {
      return;
    }

    if (disableAnimation) {
      options.showAnimationDuration = 0;
    }

    // eslint-disable-next-line
    // console.log('items', items);

    // Pass data to PhotoSwipe and initialize it
    gallery = new PhotoSwipe(pswpElement, PhotoSwipeUI_Default, items, options);
    gallery.init();
  };

  // loop through all gallery elements and bind events
  const galleryElements = document.querySelectorAll(gallerySelector);

  for (var i = 0, l = galleryElements.length; i < l; i++) {
    galleryElements[i].setAttribute('data-pswp-uid', i + 1);
    galleryElements[i].onclick = onThumbnailsClick;
  }

  // Parse URL and open gallery if it contains #&pid=3&gid=1
  var hashData = photoswipeParseHash();
  if (hashData.pid && hashData.gid) {
    openPhotoSwipe(hashData.pid, galleryElements[hashData.gid - 1], true, true);
  }
};

// execute above function
export default initPhotoSwipeFromDOM;
