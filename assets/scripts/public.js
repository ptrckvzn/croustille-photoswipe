import initPhotoSwipeFromDOM from './photoswipe';

((window) => {
  document.addEventListener("DOMContentLoaded", () => {
    const galleryClass = window.Croustille_PhotoSwipe.containerClass;

    initPhotoSwipeFromDOM(galleryClass);
  });
})(window)
