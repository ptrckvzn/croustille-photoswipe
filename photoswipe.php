<?php

namespace Croustille\PhotoSwipe;

/**
 * @wordpress-plugin
 * Plugin Name:       Croustille PhotoSwipe
 * Plugin URI:        http://example.com/photoswipe-uri/
 * Description:       Intégration de PhotoSwipe
 * Version:           0.5.0
 * Author:            Croustille
 * Author URI:        https://croustille.io/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       photoswipe
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'CROUSTILLE_PHOTOSWIPE_VERSION', '1.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-photoswipe-activator.php
 */
function activate_plugin_name() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-photoswipe-activator.php';
	Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-photoswipe-deactivator.php
 */
function deactivate_plugin_name() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-photoswipe-deactivator.php';
	Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'Croustille\\PhotoSwipe\\activate_plugin_name' );
register_deactivation_hook( __FILE__, 'Croustille\\\PhotoSwipe\\deactivate_plugin_name' );

/**
 * Load assets helper from 'dist/'
 *
 * Use: asset_path('styles/public.css');
 */
require_once plugin_dir_path( __FILE__ ) . 'includes/interface-photoswipe-manifest.php';
require_once plugin_dir_path( __FILE__ ) . 'includes/class-photoswipe-json-manifest.php';

function asset_path ($asset) {
    static $assets;
    isset($assets) || $assets = new JsonManifest(
        plugin_dir_path( __FILE__ ) . 'dist/assets.json',
        plugin_dir_url( __FILE__ ) . 'dist'
    );
    return $assets->getUri($asset);
}

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-photoswipe.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_plugin_name() {

	$plugin = new Plugin();
	$plugin->run();

}
run_plugin_name();


/**
 * Template Tags helper
 *
 * API Croustille_PhotoSwipe()->figure()
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-photoswipe-theme-api.php';

function Croustille_PhotoSwipe () {
    static $tags;
    isset($tags) || $tags = new Theme_API();
    return $tags;
}


// in theme setup.php

// add_filter('croustille/use_photoswipe', function() {
//     return !in_array(true, [
//         // Will NOT use if ANY of the following return true.
//         !\App\is_articles(),
//     ]);
// });

// add_filter('croustille/photoswipe_styles', function() {
//     return false;
// });

// add_filter('croustile/photoswipe_container_class', function() {
//     return 'article-content';
// });
