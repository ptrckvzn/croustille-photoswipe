<?php

namespace Croustille\PhotoSwipe;

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Plugin
 * @subpackage Plugin/admin
 * @author     Your Name <email@example.com>
 */
class Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

    /**
     * Ajoute l'attribut data-size au lien lors de l'ajout d'image par la boite média
     *
     * TODO: Ajouter un option 'photoswipe' dans l'éditeur media
     * afin que le lien vers l'image 'full' se fasse automatiquement
     *
     * @param string       $html    The image HTML markup to send.
     * @param int          $id      The attachment id.
     * @param string       $caption The image caption.
     * @param string       $title   The image title.
     * @param string       $align   The image alignment.
     * @param string       $url     The image source URL.
     * @param string|array $size    Size of image. Image size or array of width and height values
     *                              (in that order). Default 'medium'.
     * @param string       $alt     The image alternative, or alt, text.
     * @return void
     */
    public function send_to_editor($html, $id, $caption, $title, $align, $url, $size, $alt)
    {

        if ($url && false === strpos( $image, ' data-size=')) {
            list( $img_src, $width, $height ) = image_downsize($id, 'full');
            $data_size = sprintf(' data-size="%sx%s" href=', $width, $height);
            $html = str_replace(' href=', $data_size, $html);
        }

        return $html;

    }

    /**
     * Enlève la fonction par défaut d'affichage de la boite 'Image Details'
     *
     * https://wordpress.stackexchange.com/questions/215979/wp-media-view-imagedetails-save-settings-as-html5-data-attributes-for-image
     *
     * @return void
     */
    public function remove_default_wp_print_media_templates() {
        remove_action( 'admin_footer', 'wp_print_media_templates' );
        add_action('admin_footer', [$this, 'croustille_wp_print_media_templates']);
    }

    /**
     * Fonction de remplacement pour wp_print_media_templates
     * Ajoute un bloc 'PhotoSwipe' avec une option pour ajouter
     * l'attribut data-size
     *
     * @return void
     */
    public function croustille_wp_print_media_templates () {
        ob_start();
        \wp_print_media_templates();
        $tpl = ob_get_clean();
        // To future-proof a bit, search first for the template and then for the section.
        if ( ( $idx = strpos( $tpl, 'tmpl-image-details' ) ) !== false
                && ( $before_idx = strpos( $tpl, '<div class="advanced-section">', $idx ) ) !== false ) {
            ob_start();
            ?>
            <div class="cPhotoswipe-section">
                <h2><?php _e( 'PhotoSwipe' ); ?></h2>
                <div class="cPhotoswipe">
                    <label class="setting cPhotoswipe">
                        <span></span>
                        <input type="checkbox" data-setting="cPhotoswipe" value="{{ data.model.cPhotoswipe }}" />
                        <?php _e( 'Add data-size attribute to link' ); ?>
                    </label>
                </div>
            </div>
            <?php
            $my_section = ob_get_clean();
            $tpl = substr_replace( $tpl, $my_section, $before_idx, 0 );
        }
        echo $tpl;
    }

    /**
     * Ajoute ou non l'attribut data-size
     *
     * PROBLEME : Le data-size devrait être pour l'image 'large' et non pas la même dimension que l'image affichée :(
     *
     * @return void
     */
    public function wp_media_adjust_link_with_data_size ()
    {
        add_action( 'admin_footer', function () {
            ?>
            <script type="text/javascript">
            jQuery(function ($) {
                if (wp && wp.media && wp.media.events) {
                    wp.media.events.on( 'editor:image-edit', function (data) {
                        if ( data.image.parentNode && data.image.parentNode.nodeName === 'A' ) {
                            link = data.image.parentNode;
                            data.metadata.cPhotoswipe = data.editor.dom.getAttrib( link, 'data-size' ) ? true : false;
                        }
                    } );
                    wp.media.events.on( 'editor:image-update', function (data) {
                        if ( data.image.parentNode && data.image.parentNode.nodeName === 'A' ) {
                            link = data.image.parentNode;
                            attachment = wp.media.attachment(data.metadata.attachment_id);
                            img = attachment.attributes.sizes.full;

                            if (data.metadata.cPhotoswipe) {
                                data.editor.dom.setAttrib(link, 'data-size', `${img.width}x${img.height}`);
                            } else {
                                data.editor.dom.setAttrib(link, 'data-size', '');
                            }
                        }
                    });
                }
            });
            </script>
            <?php
        }, 11 );
    }

}
