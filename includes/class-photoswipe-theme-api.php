<?php

namespace Croustille\PhotoSwipe;

class Theme_API {
    protected $use_photoswipe;

    /* devrait probablement utiliser des fonctions wp pour créer un caption (shortcode) et généré un img et un a */
    public function figure ($id, $size = 'medium')
    {
      $image = get_post($id);
      $image_title = $image->post_title;
      $excerpt = apply_filters('croustille/photoswipe_caption', $image->post_excerpt, $id);
      $image_caption = $excerpt ? sprintf('<figcaption>%s</figcaption>', $excerpt) : '';

      $figure = sprintf(
          '<figure class="attachment_%s attachment-pswp">%s%s</figure>',
          $id,
          $this->getImageWithLink($id, $size),
          $image_caption
      );
      return $figure;
    }

    public function getImageWithLink($id, $size = 'medium')
    {

        $image = wp_get_attachment_image_src($id, 'full');

        return sprintf('<a href="%s" data-size="%sx%s">%s</a>', $image[0], $image[1], $image[2], wp_get_attachment_image($id, $size));

    }

    public function use_photoswipe()
    {
        isset($this->use_photoswipe) || $this->use_photoswipe = apply_filters('croustille/use_photoswipe', false);
        return $this->use_photoswipe;
    }
}
